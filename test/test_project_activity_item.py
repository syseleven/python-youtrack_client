"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.activity_category import ActivityCategory
from youtrack_client.model.custom_field_activity_item_all_of import CustomFieldActivityItemAllOf
from youtrack_client.model.filter_field import FilterField
from youtrack_client.model.issue import Issue
from youtrack_client.model.single_value_activity_item import SingleValueActivityItem
from youtrack_client.model.user import User
globals()['ActivityCategory'] = ActivityCategory
globals()['CustomFieldActivityItemAllOf'] = CustomFieldActivityItemAllOf
globals()['FilterField'] = FilterField
globals()['Issue'] = Issue
globals()['SingleValueActivityItem'] = SingleValueActivityItem
globals()['User'] = User
from youtrack_client.model.project_activity_item import ProjectActivityItem


class TestProjectActivityItem(unittest.TestCase):
    """ProjectActivityItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testProjectActivityItem(self):
        """Test ProjectActivityItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ProjectActivityItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

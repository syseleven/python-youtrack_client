"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.issue import Issue
from youtrack_client.model.issue_comment import IssueComment
from youtrack_client.model.user import User
from youtrack_client.model.visibility import Visibility
globals()['Issue'] = Issue
globals()['IssueComment'] = IssueComment
globals()['User'] = User
globals()['Visibility'] = Visibility
from youtrack_client.model.issue_attachment import IssueAttachment


class TestIssueAttachment(unittest.TestCase):
    """IssueAttachment unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testIssueAttachment(self):
        """Test IssueAttachment"""
        # FIXME: construct object with mandatory attributes with example values
        # model = IssueAttachment()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

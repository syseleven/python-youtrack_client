"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.activity_category import ActivityCategory
from youtrack_client.model.filter_field import FilterField
from youtrack_client.model.issue_work_item import IssueWorkItem
from youtrack_client.model.multi_value_activity_item import MultiValueActivityItem
from youtrack_client.model.user import User
from youtrack_client.model.work_item_type import WorkItemType
from youtrack_client.model.work_item_type_activity_item_all_of import WorkItemTypeActivityItemAllOf
globals()['ActivityCategory'] = ActivityCategory
globals()['FilterField'] = FilterField
globals()['IssueWorkItem'] = IssueWorkItem
globals()['MultiValueActivityItem'] = MultiValueActivityItem
globals()['User'] = User
globals()['WorkItemType'] = WorkItemType
globals()['WorkItemTypeActivityItemAllOf'] = WorkItemTypeActivityItemAllOf
from youtrack_client.model.work_item_type_activity_item import WorkItemTypeActivityItem


class TestWorkItemTypeActivityItem(unittest.TestCase):
    """WorkItemTypeActivityItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testWorkItemTypeActivityItem(self):
        """Test WorkItemTypeActivityItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = WorkItemTypeActivityItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

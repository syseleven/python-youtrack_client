"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.owned_bundle_element import OwnedBundleElement
globals()['OwnedBundleElement'] = OwnedBundleElement
from youtrack_client.model.owned_bundle_all_of import OwnedBundleAllOf


class TestOwnedBundleAllOf(unittest.TestCase):
    """OwnedBundleAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOwnedBundleAllOf(self):
        """Test OwnedBundleAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = OwnedBundleAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

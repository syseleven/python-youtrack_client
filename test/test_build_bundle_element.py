"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.build_bundle_element_all_of import BuildBundleElementAllOf
from youtrack_client.model.bundle import Bundle
from youtrack_client.model.bundle_element import BundleElement
from youtrack_client.model.field_style import FieldStyle
globals()['BuildBundleElementAllOf'] = BuildBundleElementAllOf
globals()['Bundle'] = Bundle
globals()['BundleElement'] = BundleElement
globals()['FieldStyle'] = FieldStyle
from youtrack_client.model.build_bundle_element import BuildBundleElement


class TestBuildBundleElement(unittest.TestCase):
    """BuildBundleElement unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testBuildBundleElement(self):
        """Test BuildBundleElement"""
        # FIXME: construct object with mandatory attributes with example values
        # model = BuildBundleElement()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

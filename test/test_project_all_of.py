"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.issue import Issue
from youtrack_client.model.user import User
from youtrack_client.model.user_group import UserGroup
globals()['Issue'] = Issue
globals()['User'] = User
globals()['UserGroup'] = UserGroup
from youtrack_client.model.project_all_of import ProjectAllOf


class TestProjectAllOf(unittest.TestCase):
    """ProjectAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testProjectAllOf(self):
        """Test ProjectAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ProjectAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

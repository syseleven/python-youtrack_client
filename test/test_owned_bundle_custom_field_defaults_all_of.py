"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.owned_bundle import OwnedBundle
from youtrack_client.model.owned_bundle_element import OwnedBundleElement
globals()['OwnedBundle'] = OwnedBundle
globals()['OwnedBundleElement'] = OwnedBundleElement
from youtrack_client.model.owned_bundle_custom_field_defaults_all_of import OwnedBundleCustomFieldDefaultsAllOf


class TestOwnedBundleCustomFieldDefaultsAllOf(unittest.TestCase):
    """OwnedBundleCustomFieldDefaultsAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOwnedBundleCustomFieldDefaultsAllOf(self):
        """Test OwnedBundleCustomFieldDefaultsAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = OwnedBundleCustomFieldDefaultsAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

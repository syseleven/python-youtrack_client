"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.bundle import Bundle
from youtrack_client.model.field_style import FieldStyle
from youtrack_client.model.localizable_bundle_element import LocalizableBundleElement
from youtrack_client.model.state_bundle_element_all_of import StateBundleElementAllOf
globals()['Bundle'] = Bundle
globals()['FieldStyle'] = FieldStyle
globals()['LocalizableBundleElement'] = LocalizableBundleElement
globals()['StateBundleElementAllOf'] = StateBundleElementAllOf
from youtrack_client.model.state_bundle_element import StateBundleElement


class TestStateBundleElement(unittest.TestCase):
    """StateBundleElement unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testStateBundleElement(self):
        """Test StateBundleElement"""
        # FIXME: construct object with mandatory attributes with example values
        # model = StateBundleElement()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

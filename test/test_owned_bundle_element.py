"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.bundle import Bundle
from youtrack_client.model.bundle_element import BundleElement
from youtrack_client.model.field_style import FieldStyle
from youtrack_client.model.owned_bundle_element_all_of import OwnedBundleElementAllOf
from youtrack_client.model.user import User
globals()['Bundle'] = Bundle
globals()['BundleElement'] = BundleElement
globals()['FieldStyle'] = FieldStyle
globals()['OwnedBundleElementAllOf'] = OwnedBundleElementAllOf
globals()['User'] = User
from youtrack_client.model.owned_bundle_element import OwnedBundleElement


class TestOwnedBundleElement(unittest.TestCase):
    """OwnedBundleElement unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOwnedBundleElement(self):
        """Test OwnedBundleElement"""
        # FIXME: construct object with mandatory attributes with example values
        # model = OwnedBundleElement()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.custom_filter_field import CustomFilterField
from youtrack_client.model.predefined_filter_field import PredefinedFilterField
globals()['CustomFilterField'] = CustomFilterField
globals()['PredefinedFilterField'] = PredefinedFilterField
from youtrack_client.model.filter_field import FilterField


class TestFilterField(unittest.TestCase):
    """FilterField unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testFilterField(self):
        """Test FilterField"""
        # FIXME: construct object with mandatory attributes with example values
        # model = FilterField()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.issue_tag import IssueTag
from youtrack_client.model.saved_query import SavedQuery
from youtrack_client.model.user import User
from youtrack_client.model.user_profiles import UserProfiles
from youtrack_client.model.vcs_unresolved_user_all_of import VcsUnresolvedUserAllOf
globals()['IssueTag'] = IssueTag
globals()['SavedQuery'] = SavedQuery
globals()['User'] = User
globals()['UserProfiles'] = UserProfiles
globals()['VcsUnresolvedUserAllOf'] = VcsUnresolvedUserAllOf
from youtrack_client.model.vcs_unresolved_user import VcsUnresolvedUser


class TestVcsUnresolvedUser(unittest.TestCase):
    """VcsUnresolvedUser unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testVcsUnresolvedUser(self):
        """Test VcsUnresolvedUser"""
        # FIXME: construct object with mandatory attributes with example values
        # model = VcsUnresolvedUser()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.activity_category import ActivityCategory
from youtrack_client.model.filter_field import FilterField
from youtrack_client.model.issue_work_item import IssueWorkItem
from youtrack_client.model.single_value_activity_item import SingleValueActivityItem
from youtrack_client.model.user import User
from youtrack_client.model.work_item_author_activity_item_all_of import WorkItemAuthorActivityItemAllOf
globals()['ActivityCategory'] = ActivityCategory
globals()['FilterField'] = FilterField
globals()['IssueWorkItem'] = IssueWorkItem
globals()['SingleValueActivityItem'] = SingleValueActivityItem
globals()['User'] = User
globals()['WorkItemAuthorActivityItemAllOf'] = WorkItemAuthorActivityItemAllOf
from youtrack_client.model.work_item_author_activity_item import WorkItemAuthorActivityItem


class TestWorkItemAuthorActivityItem(unittest.TestCase):
    """WorkItemAuthorActivityItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testWorkItemAuthorActivityItem(self):
        """Test WorkItemAuthorActivityItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = WorkItemAuthorActivityItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

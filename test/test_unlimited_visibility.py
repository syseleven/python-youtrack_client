"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.visibility import Visibility
globals()['Visibility'] = Visibility
from youtrack_client.model.unlimited_visibility import UnlimitedVisibility


class TestUnlimitedVisibility(unittest.TestCase):
    """UnlimitedVisibility unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testUnlimitedVisibility(self):
        """Test UnlimitedVisibility"""
        # FIXME: construct object with mandatory attributes with example values
        # model = UnlimitedVisibility()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

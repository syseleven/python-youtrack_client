"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.article_comment import ArticleComment
from youtrack_client.model.base_article import BaseArticle
from youtrack_client.model.user import User
from youtrack_client.model.visibility import Visibility
globals()['ArticleComment'] = ArticleComment
globals()['BaseArticle'] = BaseArticle
globals()['User'] = User
globals()['Visibility'] = Visibility
from youtrack_client.model.article_attachment import ArticleAttachment


class TestArticleAttachment(unittest.TestCase):
    """ArticleAttachment unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testArticleAttachment(self):
        """Test ArticleAttachment"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ArticleAttachment()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

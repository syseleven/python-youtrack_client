"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.issue import Issue
from youtrack_client.model.issue_tag import IssueTag
globals()['Issue'] = Issue
globals()['IssueTag'] = IssueTag
from youtrack_client.model.tags_activity_item_all_of import TagsActivityItemAllOf


class TestTagsActivityItemAllOf(unittest.TestCase):
    """TagsActivityItemAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTagsActivityItemAllOf(self):
        """Test TagsActivityItemAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = TagsActivityItemAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

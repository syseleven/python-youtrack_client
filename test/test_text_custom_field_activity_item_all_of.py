"""
    YouTrack REST API

    YouTrack issue tracking and project management system  # noqa: E501

    The version of the OpenAPI document: 2021.4
    Contact: info@syseleven.de
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import youtrack_client
from youtrack_client.model.issue import Issue
globals()['Issue'] = Issue
from youtrack_client.model.text_custom_field_activity_item_all_of import TextCustomFieldActivityItemAllOf


class TestTextCustomFieldActivityItemAllOf(unittest.TestCase):
    """TextCustomFieldActivityItemAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTextCustomFieldActivityItemAllOf(self):
        """Test TextCustomFieldActivityItemAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = TextCustomFieldActivityItemAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

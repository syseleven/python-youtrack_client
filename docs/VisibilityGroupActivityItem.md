# VisibilityGroupActivityItem

Represents the event when a user adds or removes a group to/from the Visibility settings of the target entity.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_member** | **str** |  | [optional] [readonly] 
**target_sub_member** | **str** |  | [optional] [readonly] 
**removed** | [**[UserGroup]**](UserGroup.md) |  | [optional] [readonly] 
**added** | [**[UserGroup]**](UserGroup.md) |  | [optional] [readonly] 
**author** | [**User**](User.md) |  | [optional] 
**timestamp** | **int** |  | [optional] [readonly] 
**target** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 
**field** | [**FilterField**](FilterField.md) |  | [optional] 
**category** | [**ActivityCategory**](ActivityCategory.md) |  | [optional] 
**id** | **str** |  | [optional] [readonly] 
**type** | **str** |  | [optional] [readonly] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



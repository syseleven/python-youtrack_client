# CommentActivityItem

Represents a change in the comments of an issue.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target** | [**IssueComment**](IssueComment.md) |  | [optional] 
**removed** | [**[IssueComment]**](IssueComment.md) |  | [optional] [readonly] 
**added** | [**[IssueComment]**](IssueComment.md) |  | [optional] [readonly] 
**author_group** | [**UserGroup**](UserGroup.md) |  | [optional] 
**author** | [**User**](User.md) |  | [optional] 
**timestamp** | **int** |  | [optional] [readonly] 
**target_member** | **str** |  | [optional] [readonly] 
**field** | [**FilterField**](FilterField.md) |  | [optional] 
**category** | [**ActivityCategory**](ActivityCategory.md) |  | [optional] 
**id** | **str** |  | [optional] [readonly] 
**type** | **str** |  | [optional] [readonly] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



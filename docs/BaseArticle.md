# BaseArticle

Represents a base article entity.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reporter** | [**User**](User.md) |  | [optional] 
**visibility** | [**Visibility**](Visibility.md) |  | [optional] 
**summary** | **str** |  | [optional] 
**content** | **str** |  | [optional] 
**attachments** | [**[ArticleAttachment]**](ArticleAttachment.md) |  | [optional] 
**id** | **str** |  | [optional] [readonly] 
**type** | **str** |  | [optional] [readonly] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



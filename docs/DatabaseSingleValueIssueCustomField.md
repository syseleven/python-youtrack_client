# DatabaseSingleValueIssueCustomField

Represents the all fields of with a single value in the issue.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 
**name** | **str** |  | [optional] [readonly] 
**project_custom_field** | [**ProjectCustomField**](ProjectCustomField.md) |  | [optional] 
**id** | **str** |  | [optional] [readonly] 
**type** | **str** |  | [optional] [readonly] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



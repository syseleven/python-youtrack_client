# ArticleAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reporter** | [**User**](User.md) |  | [optional] 
**visibility** | [**Visibility**](Visibility.md) |  | [optional] 
**summary** | **str** |  | [optional] 
**content** | **str** |  | [optional] 
**attachments** | [**[ArticleAttachment]**](ArticleAttachment.md) |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**parent_article** | [**Article**](Article.md) |  | [optional] 
**child_articles** | [**[Article]**](Article.md) |  | [optional] 
**has_children** | **bool** |  | [optional] [readonly] 
**updated_by** | [**User**](User.md) |  | [optional] 
**updated** | **int** |  | [optional] [readonly] 
**created** | **int** |  | [optional] [readonly] 
**id_readable** | **str** |  | [optional] [readonly] 
**ordinal** | **int** |  | [optional] [readonly] 
**comments** | [**[ArticleComment]**](ArticleComment.md) |  | [optional] 
**has_star** | **bool** |  | [optional] 
**external_article** | [**ExternalArticle**](ExternalArticle.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


